/*jslint todo: true */
/*global console, alert*/

var debug = false;
if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function (suffix) {
        "use strict";
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}
(function ($) {
    "use strict";
    var _methods = {//internal methods
        newTag : function (tagName, ct, cl, st) {
            var obj =  $(document.createElement(tagName));
            if (cl !== undefined && cl !== null) {
                obj.addClass(cl);
            }
            if (st !== undefined && st !== null) {
                obj.css(st);
            }
            if (ct !== undefined && ct !== null) {
                obj.append(ct);
            }
            return obj;
        },
        getValue : function (inVal) {
            var outVal = $.trim(inVal);

            return outVal;
        },
        checkDPVisibility : function (input) {
            if ($('#ui-datepicker-div').css('display') === 'none') {
                input.trigger('change');
            } else {
                setTimeout(function () {_methods.checkDPVisibility(input); }, 100);
            }
        },
        fillSelect: function (obj, jsonData) {
            var select = $(obj).find('select');
            var fVal = obj.data('value');
            $(jsonData).each(function () {
                var option, value, fragments, i, label;
                option = this;
                value = option.value;

                fragments = [];
                for (i = 1; option[i] !== undefined; i = i + 1) {
                    fragments.push(option[i]);
                }
                label = fragments.join(' | ');
                var h = _methods.newTag('option', label)
                    .attr('value', value);
                if(fVal[0] === value){
                    h.attr('selected', 'selected');
                }
                select.append(h);
            });
            if(obj.data('all').editType == 'combobox'){
                select.combobox();
                var field = $(obj.children()[1]);
                var cw = obj.css('width');
                field.css('width', cw.substr(0, cw.indexOf('px')) - 27);
                $(obj.children()[2]).css('width', 18);

                var input = $(field.children()[1]);
                input.on('blur', _methods.saveField);
                input.focus();
                input.select();
                input.on('keypress', _methods.tab);

                var comboboxButton = $(field.find('a.ui-button')[0]);
                comboboxButton.on('mousedown', function (event) {
                    var $this, input, dp;
                    $this = $(this);
                    input = $this.parent().parent().find('input');
                    input.off('blur');//deactivate the event
                    input.on('autocompleteclose', function () {
                        $this = $(this);
                        $this.on('blur', _methods.saveField);//reactivate the event
                        $this.focus();
                        $this.select();
                    });
                });

            }
        },
        edit : function (event, obj) {
            var td, fieldDef, val, bloc, settings, row, field, datePickerButton, selected, cw, url, select;
            td = obj || $(this);
            fieldDef = td.data('all');
            val = td.data('value');
            bloc = td.parent().parent().parent().parent().parent().parent().parent().parent();
            settings = bloc.data('settings');

            if (td.data('inEdition')) {
                return;
            }
            td.data('inEdition', true);

            row = td.parent().first();
            if (!row.data('inEdition')) {
                row.data('inEdition', true);
                //row.on('blur', _methods.persistRow);
            }

            td.empty();

            switch (fieldDef.editType) {
                case 'datepicker':
                    field = _methods.newTag('input');
                    field.attr('type', 'text');
                    field.on('blur', _methods.saveField);
                    td.append(field);
                    field.datepicker({
                        dateFormat: 'dd.mm.yy',
                        showOn: 'button',
                        buttonImageOnly: true,
                        buttonImage: '/bgrid/lib/jquery/images/calendar.gif'
                    });
                    if (val !== undefined) {
                        field.datepicker('setDate', val);
                    }
                    field.focus();
                    field.select();

                    datePickerButton = td.find('.ui-datepicker-trigger');
                    datePickerButton.on('mousedown', function (event) {
                        var $this, input, dp;
                        $this = $(this);
                        input = $this.parent().find('input');
                        input.off('blur');//deactivate the event
                        input.on('change', function () {
                            $this = $(this);
                            $this.on('blur', _methods.saveField);//reactivate the event
                            $this.focus();
                        });

                        setTimeout(function () {
                            _methods.checkDPVisibility(input);
                        }, 1000);
                        //probablement Ã  effacer (ci-dessous)
                        dp = $('ui-datepicker-div');
                        if (dp.length === 1) {
                            dp.on('hide', function (event) {
                                var a = 'ss';
                            });
                        } else {
                            $this.on('mouseup', function (event) {
                                $('ui-datepicker-div').on('hide', function (event) {
                                    // TODO: what was this for?!?
                                    // var a = 'ss';
                                });
                            });
                        }
                    });
                    break;
                case 'combobox':
                case 'select':
                    field = _methods.newTag('select');
                    if (val !== undefined) {
                        selected = field.children().filter('[value=' + val[0] + ']');
                        if (selected.length > 0) {
                            selected.attr('selected', 'selected');
                        }
                    }
                    td.append(field);

                    if (fieldDef.editType === 'combobox') {
                        field.hide();
                        //field.combobox();
                        //field = $(td.children()[1]);
                        //cw = td.css('width');
                        //field.css('width', cw.substr(0, cw.indexOf('px')) - 27);
                        //$(td.children()[2]).css('width', 20);
                    }
                    field.on('blur', _methods.saveField);

                    url = settings.crudUrl.replace('crud', 'select');
                    var selectData = bloc.data('selectData.' + fieldDef.name);
                    if (selectData !== undefined) {
                        _methods.fillSelect(td, selectData);
                    } else {
                        $.ajax(url + '/' + fieldDef.name, {context: td})
                            .done(function (txtData, b, c) {
                                if (debug) {
                                    console.debug('ajax query for select options done context: ' + $(this).attr('id') + " - and data : " + txtData);
                                }
                                var jsonData = $.parseJSON(txtData);
                                var bloc = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
                                bloc.data('selectData.' + $(this).data('all').name, jsonData);
                                _methods.fillSelect(this, jsonData);
                            })
                            .fail(function (a, b, c) {
                                alert('Le contenu du select n\' a pas pu Ãªtre rÃ©cupÃ©rÃ©');
                            });
                    }

                    field.focus();
                    break;
                case 'radio':

                    break;
                case 'checkbox':

                    break;
                //case 'text': //Like default
                default:
                    field = _methods.newTag('input');
                    field.attr('type', 'text');
                    field.attr('value', val);
                    field.on('blur', _methods.saveField);

                    td.append(field);
                    field.focus();
                    field.select();
                    break;
            }
            field.on('keypress', _methods.tab);
            if (debug) {
                console.debug('edit done.');
            }
        },
        saveField : function (event) {
            var field, container, newVal, oldVal, val, dateParts, label, editType, ct;
            field = $(this);
            ct = field;
            do{
                container = ct = ct.parent();
                if(container.length === 0)return;
            } while(container.data('all') === undefined )
            editType = container.data('all').editType;
            switch (editType) {
                case 'text':
                case 'textarea':
                case 'number':
                    newVal = _methods.getValue(field.val());
                    oldVal = container.data('value');
                    if (newVal === oldVal || (!newVal && !oldVal)/*both are empty string or null*/) {
                        if (!oldVal) {
                            val = '&nbsp;';
                        } else {
                            val = oldVal;
                        }
                    } else {
                        container.parent().data('modified', true);
                        container.data('value', newVal);
                        val = newVal;
                    }
                    container.empty();
                    container.append(val);
                    container.data('inEdition', false);
                    break;
                case 'combobox':
                    oldVal = container.data('value');
                    field = $(container.find('select')[0]);
                    newVal = field.val();
                    label = field.children().filter("option[value="+ newVal +"]").text();
                    if (oldVal !== undefined && newVal === oldVal[0]) {
                        val = oldVal;
                    } else {
                        container.parent().data('modified', true);
                        val = [newVal, label];
                        container.data('value', val);
                    }
                    container.empty();
                    container.append(val[1]);
                    container.data('inEdition', false);
                    break;
                case 'select':
                    oldVal = container.data('value');
                    field = $(container.find('select')[0]);
                    newVal = field.val();
                    label = field.children().filter("option[selected=selected]").text();
                    if (oldVal !== undefined && newVal === oldVal[0]) {
                        val = oldVal;
                    } else {
                        container.parent().data('modified', true);
                        val = [newVal, label];
                        container.data('value', val);
                    }
                    container.empty();
                    container.append(val[1]);
                    container.data('inEdition', false);
                    break;
                case 'datepicker':
                    dateParts = field.val().split('.');
                    newVal = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
                    oldVal = container.data('value') === undefined ? null : container.data('value');
                    if (oldVal !== null && newVal.getTime() === oldVal.getTime()) {
                        if (oldVal === '') {
                            val = '&nbsp;';
                        } else {
                            val = oldVal.format('dd.mm.yyyy');
                        }

                    } else {
                        container.parent().data('modified', true);
                        //var dateParts = newVal.split('.');
                        //var newDate = new Date(dateParts[2], dateParts[1]-1/*zero based*/, dateParts[0]);
                        container.data('value', newVal);
                        val = newVal.format('dd.mm.yyyy');
                    }
                    container.empty();
                    container.append(val);
                    container.data('inEdition', false);
                    break;
                default:
                    alert("Unknown type " + editType);
                    break;
            }


            //setTimeout(function () {_methods.persistRow(container); }, 100);//under 100ms the focus is not always set on new element.
        },
        persistRow : function (container) {
            var tr, bloc, settings, focused, requestType, id, data, url, index, saveTag, fieldNames;
            tr = container.parent();
            if (tr.parent()[0].tagName === 'THEAD'){
                tr.parent().parent().children('tbody').children().each(function () {
                    var $this = $(this);
                    if ($this.data('modified')) {
                        _methods.persistRow($($(this).children()[0]));
                    }
                });
                return;
            }
            bloc = tr.parent().parent().parent().parent().parent().parent().parent();
            settings = bloc.data('settings');
            focused = document.activeElement;
            if (tr.find(focused).length) {
                focused.blur();
            }
            if (!tr.data('modified')) {
                return;
            }

            requestType = tr.data('newRecord') ? 'POST' : 'PUT';

            id = 0;
            data = {};

            data.fields = [];
            $(settings.fields).each(function () {
                data.fields.push(this.name);
            });

            tr.children('.dataField').each(function () {
                var $this = $(this),
                    fieldDef = $this.data('all'),
                    fieldName = fieldDef.name,
                    fieldValue = $this.data('value');
                if (fieldValue === undefined) {
                    fieldValue = fieldDef.default;
                }
                if (fieldValue !== undefined) {
                    if (fieldName.indexOf('/') > -1 && fieldDef.editable){
                        index = fieldName.indexOf('/');
                        fieldName = fieldName.substr(0, index);
                        fieldValue = fieldValue[0];
                        data[fieldName] = fieldValue;
                    }
                    if (fieldName === "id") {
                        id = fieldValue;
                    } else if (fieldDef.editable) {
                        data[fieldName] = fieldValue;
                    }
                }
            });
            //Move that in ajax.done function (will have to search for tr)
            url = settings.crudUrl;
            if (!tr.data('newRecord')) {
                url += "/" + id;
            }

            saveTag = _methods.newTag('td', 'Sauvegarde en cours..', 'ctrl-msg')
                .css({position: 'absolute', left: '0'});
            tr.append(saveTag);

            $.ajax(url, {
                data: data,
                type: requestType,
                context: tr
            })
                .done(function (txtData, b, c) {
                    var tr = $(this);
                    tr.find('.ctrl-msg').remove();
                    var jsonData = $.parseJSON(txtData);
                    if (txtData === 'true') {
                        tr.data('modified', false);
                    } else {
                        _methods.fillRow(jsonData, tr);
                        tr.data('modified', false);
                    }
                    //_methods.setData(grid, jsonData);
                })
                .fail(function (a, b, c){
                    alert('La requÃªte ajax n\'a pas abouti');
                });
        },
        createUi : function (obj) {
            var settings = obj.data('settings');
            var _settings = obj.data('_settings');

            obj.addClass('ui-widget bloc ui-corner-all')
                .css({'width': typeof settings.width === 'string' && settings.width.endsWith('%') ? settings.width : settings.width + 'px', 'height': settings.height + 'px'})
                .append(_methods.newTag('div', null, 'ui-bgrid-header ui-widget-header ui-corner-all', {display : settings.topNavDisplay})
                    //.append(_methods.newTag('div', null, 'left nav top ui-icon ui-icon-arrowstop-1-w ui-corner-tl ui-corner-bl'))
                    //.append(_methods.newTag('div', null, 'left nav top ui-icon ui-icon-arrow-1-w ui-corner-tr ui-corner-br'))
                    //.append(_methods.newTag('div', null, 'right nav top ui-icon ui-icon-arrowstop-1-e ui-corner-tr ui-corner-br'))
                    //.append(_methods.newTag('div', null, 'right nav top ui-icon ui-icon-arrow-1-e ui-corner-tl ui-corner-bl'))
                    .append(_methods.newTag('div', settings.title, 'ui-bgrid-title')
                        .append(_methods.newTag('div', null, 'pull-right')))
                ).append(_methods.newTag('div', null, 'ui-helper-clearfix'))
                .append(_methods.newTag('div', null, 'ui-bgrid-predata ui-corner-all', {display : settings.preDataDisplay}))
                .append(_methods.newTag('div', null, null, {'height' : _settings.dataHeight + 'px'})
                    .append(_methods.newTag('div', null, 'ui-bgrid-leftzone', {display : _settings.leftZoneDisplay}))
                    .append(_methods.newTag('div', null, 'ui-bgrid-rightzone ui-corner-all', {display : _settings.rightZoneDisplay}))
                    .append(_methods.newTag('div', null, 'content')
                        .append(_methods.newTag('div', null, 'grid', {height: (_settings.dataHeight - 40) + 'px'}))
                    ).append(_methods.newTag('div', null, 'ui-helper-clearfix'))
                ).append(_methods.newTag('div', null, 'ui-bgrid-postdata ui-corner-all', {display : settings.postDataDisplay}))
                .append(_methods.newTag('div', '&nbsp;', 'ui-bgrid-header ui-widget-header ui-corner-all', {display : settings.bottomNavDisplay})
                    .append(_methods.newTag('div', null, 'left nav top ui-icon ui-icon-arrowstop-1-w ui-corner-tl ui-corner-bl'))
                    .append(_methods.newTag('div', null, 'left nav top ui-icon ui-icon-arrow-1-w ui-corner-tr ui-corner-br'))
                    .append(_methods.newTag('div', null, 'right nav top ui-icon ui-icon-arrowstop-1-e ui-corner-tr ui-corner-br'))
                    .append(_methods.newTag('div', null, 'right nav top ui-icon ui-icon-arrow-1-e ui-corner-tl ui-corner-bl'))
                    .append(_methods.newTag('div', 'Foot text', 'ui-bgrid-footer'))
                ).append(_methods.newTag('div', null, 'ui-helper-clearfix'));

        },
        setData : function (data, obj) {
            var $this = obj === null ? $(this) : $(obj),
                grid = $this.find('.grid'),
                settings = $this.data('settings'),
                _settings = $this.data('_settings');
            if (debug) {
                console.debug('context in setData : ' + $this.attr('id') + " - and data : " + data);
            }
            grid.empty();
            grid.data('records', data);
            grid.append(_methods.newTag('div', null, 'in', {'height': (_settings.dataHeight - 40) + 'px'})
                .append(_methods.newTag('table')
                    .append(_methods.newTag('caption'))
                    .append(_methods.newTag('thead')
                        .append(_methods.newTag('tr')))
                    .append(_methods.newTag('tbody')
                        //.append(_methods.newTag('tr'))
                    )));
            if (settings.selection !== 'none'){
                grid.find('tr').first().append(_methods.newTag('th', null, 'selector'));
            }
            if (settings.allowDeletions){
                grid.find('tr').first().append(_methods.newTag('th', null, 'selector'));
            }
            $(settings.fields).each(function () {
                if (!this.label) var label = '&nbsp;';
                else var label = this.label;
                var newTag = _methods.newTag('th', label, null,{width:this.width})
                if(this.hidden){
                    newTag.hide();
                }
                newTag.data('all',this);
                grid.find('tr').first().append(newTag);
            });

            if(settings.actions !== undefined){
                $(settings.actions).each(function(){
                    if(this.pos === 'left'){
                        grid.find('tr').first().prepend(_methods.newTag('th','&nbsp;',null,{width:'17px;'}));
                    }
                    else if(this.pos === 'right'){
                        grid.find('tr').first().append(_methods.newTag('th','&nbsp;',null,{width:'17px'}));
                    }
                    else{//whole row

                    }
                });
            }

            $(grid.data('records')).each(function(){
                _methods.addRow(this, grid);
            });
            if(settings.selection !== 'none'){
                $("input[name='"+$this.attr('id')+"Selection']")
                    .on('change', function(){
                        $this = $(this);
                        var bloc = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent();
                        var tr = $(this).parent().parent();
                        bloc.data('value', tr.data('value'));
                    });
            }
        },
        dateIsoToCh : function(iso){
            var parts = iso.split('-'),
                ch = parts[2]+'.'+parts[1]+'.'+parts[0];
            return ch;
        },
        tab : function(event){
            if(event.keyCode === 9){//9 = tabulator
                var field = $(this);
                var container = field;
                do{
                    container = container.parent();
                }while(container.length === 1 && container[0].tagName !== 'TD')

                var nextContainer = container;
                var ctContainer = container;
                while(nextContainer = event.shiftKey ?
                    nextContainer.prev('.dataField') :
                    nextContainer.next('.dataField')){
                    var isLastField = !nextContainer[0];
                    if(isLastField){
                        if(debug)console.debug('go to next row.');
                        var nextRow = event.shiftKey ? ctContainer.parent().prev() :  ctContainer.parent().next();
                        if(nextRow.length == 0){
                            if(event.shiftKey){
                                return false;
                            }
                            _methods.addRow(null, ctContainer.parent().parent().parent().parent().parent());
                            nextContainer = ctContainer;
                        }
                        else{
                            nextContainer = event.shiftKey ?
                                nextRow.children('.dataField').last() :
                                nextRow.children('.dataField').first();

                            var nextData = nextContainer.data('all');
                            if(nextData.editable && !nextData.hidden){
                                nextContainer.trigger('click');
                                return false;
                            }
                        }
                    }
                    else if(nextContainer.data('all').editable && !nextContainer.data('all').hidden){
                        nextContainer.trigger('click');
                        return false;
                    }
                    ctContainer = nextContainer;
                }
                if(debug)console.debug('end of tab.');
            } else if (event.keyCode === 13 && event.ctrlKey) {//enter
                _methods.persistRow($(this).parent('td'));
            } else if (event.keyCode === 27){//Escape
                var row = $(this).parent().parent();
                //row.find('.icon-remove ctrl-btn').trigger('click');
                $(this).blur();
            }
        },
        fillRow : function(data, row){
            var grid, settings, fieldIndex, cell;
            grid = row.parent().parent().parent().parent().parent().parent().parent();
            settings = grid.data('settings');
            row.data('all', data[0]);
            fieldIndex = 0;
            if(settings.selection === 'unique' || settings.selection ==='multiple') fieldIndex++;
            if(settings.allowDeletions) fieldIndex++;

            $(settings.fields).each(function(){
                var name = this.name;
                if(name.indexOf("/") !== -1){
                    var lastSlashPos = name.lastIndexOf("/");
                    var cutSlashPos = name.substr(0,lastSlashPos-1).lastIndexOf("/");
                    var valKey = name.substring(cutSlashPos+1, lastSlashPos);
                    var val = new Array(row.data('all')[valKey+":value"],row.data('all')[valKey+":label"]);
                }
                else{
                    var val = row.data('all')[name];
                }

                cell = $(row.children()[fieldIndex++]);

                var disp = _methods.setCellValue(val, cell);
                cell.text(disp);
            });
        },
        setCellValue : function(val, cell){
            cell.data('value', val);

            if(cell.data('all').isKey){
                cell.parent().data('value', val);
            }
            var disp;

            if(Object.prototype.toString.call(val) === '[object Array]'){
                if(val.length >= 2){
                    disp = val[1];
                }
                else{
                    alert('Array size is 1. 2 is expected at least.');
                }
            }
            else{
                switch (cell.data('all').dataType)
                {
                    case 'date':
                        if(val !== undefined && val !== null){
                            if(Object.prototype.toString.call(val) !== "[object Date]"){
                                var dateParts = val.split('-');
                                val = new Date(dateParts[0],dateParts[1]-1, dateParts[2]);
                                var rData = cell.data('value', val);
                            }
                            disp = val.format("dd.mm.yyyy");
                        }
                        break;
                    case 'bool':
                        if(val === 1){
                            disp = 'oui';
                        }
                        else{
                            disp = 'non';
                        }
                        break;
                    default:// 'number', 'text'
                        if(!val && val !== 0){
                            disp = '&nbsp;';
                        }
                        else{
                            disp = val;
                        }
                        break
                }
            }
            return disp;
        },
        addRow : function(data, grid) {
            var record = _methods.newTag('tr');
            if(data) record.data('all',data);
            else record.data('newRecord', true);

            record.on('setData', function (e, key, value) {
                if(key === 'modified'){
                    _methods.rowStatus(this, value);
                }
            })
            var bloc = grid.parent().parent().parent();
            var settings = bloc.data('settings');
            if(settings.selection === 'unique'){
                var s = _methods.newTag('td')
                    .addClass('selector')
                    .append(_methods.newTag('input')
                        .attr('type', 'radio')
                        .attr('name', bloc.attr('id')+'Selection'));
                record.append(s);
            }
            else if(settings.selection === 'multiple'){
                var s = _methods.newTag('td')
                    .addClass('selector')
                    .append(_methods.newTag('input')
                        .attr('type', 'checkbox')
                        .attr('name', bloc.attr('id')+'Selection'));
                record.append(s);
            }
            if (settings.allowDeletions) {
                var s = _methods.newTag('td', '', 'delete icon-remove-sign', {cursor: 'pointer'})
                    .prop('title', "Double cliquer pour supprimer l'enregistrement")
                    .on('dblclick', function () {
                        var row = $(this).parent();

                        var saveTag = _methods.newTag('td', 'Suppression en cours..', 'ctrl-msg')
                            .css({position: 'absolute', left: '0'});
                        row.append(saveTag);

                        $.ajax(settings.crudUrl + '/' + row.data('value'), {context: row, type: 'DELETE'})
                            .done(function (txtData, b, c) {
                                //var jsonData = $.parseJSON(txtData);
                                if(txtData === '1'){
                                    $(this).remove();
                                }
                            })
                            .fail(function (a, b, c) {
                                alert('Le contenu du bloc n\' a pas pu Ãªtre rÃ©cupÃ©rÃ©');
                            });

                    });
                record.append(s);
            }

            var focusDefined = false;
            $(settings.fields).each(function(){
                var newValue = _methods.newTag('td', '', 'dataField');
                newValue.data('all', this);

                if(data){
                    var name = newValue.data('all').name;
                    if(name.indexOf("/") !== -1){
                        var lastSlashPos = name.lastIndexOf("/");
                        var cutSlashPos = name.substr(0,lastSlashPos-1).lastIndexOf("/");
                        var valKey = name.substring(cutSlashPos+1, lastSlashPos);
                        var val = new Array(record.data('all')[valKey+":value"],record.data('all')[valKey+":label"]);
                    }
                    else{
                        var val = record.data('all')[name];
                    }

                    newValue.data('value', val);
                }

                record.append(newValue);
                var disp = _methods.setCellValue(val, newValue);

                if(newValue.data('all').editable){
                    newValue.addClass('editable');
                    newValue.on('click', _methods.edit);
                }
                if(newValue.data('all').hidden){
                    newValue.hide();
                }
                newValue.css({width:newValue.data('all').width});

                newValue.append(disp);

                if(this.subscriptions !== null){
                    $(this.subscriptions).each(function () {
                        this(newValue);
                    });
                }
            });
            if(settings.actions != undefined){
                var eventSrc = this;
                $(settings.actions).each(function(){
                    if(this.pos === 'left'){
                        record.prepend(_methods.newTag('td',null,this.class,this.style));
                    }
                    else if(this.pos === 'right'){
                        record//.append(_methods.newTag('td')
                            .append(_methods.newTag('td',null,this.class,this.style)
                                .on(this.event, this.func)
                                .on('mouseover', function(){$(this).addClass('ui-state-hover')})
                                .on('mouseout', function(){$(this).removeClass('ui-state-hover')})
                                .attr('title', this.alt));
                    }
                    else{//whole row

                    }
                });
            }
            grid.find('tbody').append(record);
            return record;
        },
        isInt : function(n) {
            return typeof n === 'number' && n % 1 == 0;
        },
        rowStatus : function(obj, modified) {
            if(modified && $(obj).find('.ctrl-btn').length === 0){
                var btnOk = _methods.newTag('td', '', 'icon-ok ctrl-btn')
                    .prop('title', 'sauver')
                    .css({/*position: 'absolute', right: 20, */cursor: 'pointer'})
                    .on('click', function () {
                        _methods.persistRow($(this));
                    });
                var btnCancel = _methods.newTag('td', '', 'icon-remove ctrl-btn')
                    .prop('title', 'annuler')
                    .css({/*position: 'absolute', right:40, */cursor: 'pointer'})
                    .on('click', function () {
                        $(obj).data('modified', false);
                    });
                $(obj).append(btnOk);
                $(obj).append(btnCancel);
                var headerRow = $($($(obj).parent().parent().children('thead')[0]).children()[0]);
                if(headerRow.find('.ctrl-btn').length === 0){
                    //Add control buttons to header
                    var btnOkH = btnOk.clone().on('click', function () {
                        _methods.persistRow($(this))
                    });
                    var btnCancelH = btnCancel.clone().on('click', function () {

                    })
                    headerRow.append(btnOkH);
                    headerRow.append(btnCancelH);
                }

            } else if (!modified){
                $(obj).find('.ctrl-btn').remove();
            }
        },
        setButtonDisplay : function (bloc, buttonName, add){//if addd is falsethen remove
            var buttonsGroup, settings;
            buttonsGroup = $($($(bloc.children()[0]).children()[0]).children()[0]);
            settings = bloc.data('settings');

            if (add) {
                switch(buttonName){
                    case "addButton" :
                        buttonsGroup.append(_methods.newTag('span', null, 'add-button icon-plus-sign icon-white')
                            .on('mouseover', function () {$(this).removeClass('icon-white'); })
                            .on('mouseout',  function () {$(this).addClass('icon-white'); })
                            .on('click',
                            function () {
                                var bloc = $(this).parent().parent().parent().parent(),
                                    newRow = _methods.addRow(null, bloc.find('.grid')),
                                    found = false;
                                newRow.children().each(function () {
                                    var data = $(this).data('all');
                                    var $this = $(this);
                                    if (found || data === undefined) {
                                        return;
                                    }
                                    if (data.editable && !data.hidden) {
                                        found = true;
                                        _methods.edit(null, $this);
                                    }
                                });
                            }));

                        break;
                }
            } else {//remove
                buttonsGroup.find("[name='" + buttonName + "']").each(function(){
                    $(this).remove();
                });
            }
        }
    };

    var methods = {//public methods
        init : function (options) {
            return this.each(function () {
                var bloc = $(this),
                    settings = $.extend({
                        width  : 'auto',
                        height : 200,
                        topNavDisplay : 'block',
                        bottomNavDisplay : 'none',
                        preDataDisplay : 'none',//or block
                        postDataDisplay : 'none',//or block
                        editable: false,
                        type : 'grid',
                        title : '',
                        crudUrl : null,
                        entityName : null,
                        fields : null,
                        selection : 'none',//none unique multiple
                        filters : null,//[{key:'idMyForeignKey',operation:'LIKE' or '=',value:'8',isServerFilter:true}]
                        leftZone : null,
                        rightZone : null,
                        actions : null,
                        allowDeletions : false
                    }, options),
                    width,
                    _settings,
                    sub,
                    buttonsGroup;

                if (settings.width === 'auto') {
                    width = 20;//20 is the space for the scrollbar when visible.

                    if(settings.editable) width += 2*18;//18 for save/cancel buttons.

                    $(settings.fields).each(function () {
                        if (this.hidden !== true) {
                            //3 = 1 padding left, 1 padding right and 1 border right
                            width += parseInt(this.width.substring(0, this.width.length - 2), 10) + 3;
                        }
                    });
                    if (settings.actions !== null && settings.actions !== undefined) {
                        width += settings.actions.length * 20;
                    }
                    if (settings.leftZone !== null) {
                        width += 18;
                    }
                    if (settings.rightZone !== null) {
                        width += 18;
                    }
                    if (settings.selection !== 'none') {
                        width += 16;
                    }
                    if (settings.allowDeletions) {
                        width += 17;
                    }
                    settings.width = width;
                }

                _settings = {
                    dataHeight : settings.height - 80,
                    dataWidth : settings.width - 60,
                    leftZoneDisplay : 'block',
                    rightZoneDisplay : 'block'
                };

                if (settings.topNavDisplay === 'none') {
                    _settings.dataHeight += 20;
                }
                if (settings.bottomNavDisplay === 'none') {
                    _settings.dataHeight += 20;
                }
                if (settings.preDataDisplay === 'none') {
                    _settings.dataHeight += 20;
                }
                if (settings.postDataDisplay === 'none') {
                    _settings.dataHeight += 20;
                }
                if (settings.leftZone === null) {
                    _settings.dataWidth += 18;
                    _settings.leftZoneDisplay = 'none';
                }
                if (settings.rightZone === null) {
                    _settings.dataWidth += 18;
                    _settings.rightZoneDisplay = 'none';
                }

                bloc.data('settings', settings);
                bloc.data('_settings', _settings);

                _methods.createUi(bloc);

                if (settings.subscriptions !== null) {
                    sub = $(settings.subscriptions).each(function () {
                        this(bloc);
                    });
                }

                if (settings.buttons !== null && settings.buttons !== undefined) {
                    $(settings.buttons).each(function () {
                        _methods.setButtonDisplay(bloc, this, true);
                    });
                }
                bloc.bgrid('refresh');
            });
        },
        refresh : function () {
            return this.each(function () {
                var $this = $(this),
                    settings = $this.data('settings'),
                    _settings = $this.data('settings'),
                    names = new Array();
                $(settings.fields).each(function () {
                    names.push(this.name);
                });
                if (debug) {
                    console.debug('context for ajax query : ' + $this.attr('id'));
                }
                if(settings.crudUrl !== null){
                    $.ajax(settings.crudUrl, {data: {fields: names, filters: settings.filters, orderBy: settings.orderBy}, context: $this})
                        .done(function (txtData, b, c) {
                            if (debug) console.debug('ajax query done context: ' + $(this).attr('id') + " - and data : " + txtData);
                            var jsonData = $.parseJSON(txtData);
                            _methods.setData(jsonData, this);
                        })
                        .fail(function (a, b, c) {
                            alert('Le contenu du bloc n\' a pas pu Ãªtre rÃ©cupÃ©rÃ©');
                        });
                }
                else if(settings.data){
                    _methods.setData(settings.data, this);
                }
            });
        },
        setButton : function (buttonName, enable) {
            $(this).each(function () {
                var settings, isEnabled, addIndex, bloc;
                bloc = $(this);
                settings = bloc.data('settings');

                if(settings.buttons === undefined){
                    if(enable){
                        settings.buttons = [];
                    }
                    addIndex = -1;
                } else{
                    addIndex = settings.buttons.indexOf(buttonName);
                }
                isEnabled = !(addIndex === -1);
                if(!isEnabled && enable){
                    //enable
                    settings.buttons.push(buttonName);
                    _methods.setButtonDisplay(bloc, buttonName, true);
                }else if (isEnabled && !enable){
                    //disable
                    settings.buttons.splice(addIndex, 1);
                    _methods.setButtonDisplay(bloc, buttonName, false);
                }
                bloc.data('settings', settings);
            });
        }
    };

    $.fn.bgrid = function( method ) {

        // Method calling logic
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
        }

    };

})( jQuery );
